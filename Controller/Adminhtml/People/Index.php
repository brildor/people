<?php
namespace Adridope\People\Controller\Adminhtml\People;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 */
class Index extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Adridope_People::people';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/people_people_index.xml
     *
     * @return Page
     */
    public function execute() :Page
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Adridope_People::people');
        $resultPage->getConfig()->getTitle()->prepend(__('People'));

        return $resultPage;
    }
}
