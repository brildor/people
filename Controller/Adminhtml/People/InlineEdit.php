<?php declare(strict_types=1);

namespace Adridope\People\Controller\Adminhtml\People;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Adridope\People\Model\People;
use Adridope\People\Model\PeopleFactory;
use Adridope\People\Model\ResourceModel\People as PeopleResource;

class InlineEdit extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Adridope_People::people_save';

    /** @var JsonFactory */
    protected $jsonFactory;

    /** @var PeopleFactory */
    protected $peopleFactory;

    /** @var PeopleResource*/
    protected $peopleResource;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param PeopleFactory $peopleFactory
     * @param PeopleResource $peopleResource
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        PeopleFactory $peopleFactory,
        PeopleResource $peopleResource
    ){
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->peopleFactory = $peopleFactory;
        $this->peopleResource = $peopleResource;
    }

    public function execute()
    {
        $json = $this->jsonFactory->create();
        $messages = [];
        $error = false;
        $isAjax = $this->getRequest()->getParam('isAjax', false);
        $items = $this->getRequest()->getParam('items', []);

        if(!$isAjax || !count($items)){
            $messages[] = __('Please correct the data sent.');
            $error = true;
        }

        if(!$error){
            foreach ($items as $item) {
                $id = $item['id'];
                try {
                    /** @var People $people */
                    $people = $this->peopleFactory->create();
                    $this->peopleResource->load($people, $id);
                    $people->setData(array_merge($people->getData(),$item));
                    $this->peopleResource->save($people);
                }catch (\Exception $e){
                    $messages[] = "Something went wrong while saving person $id";
                    $error = true;
                }
            }
        }

        return $json->setData([
            'messages' => $messages,
            'error' => $error,
        ]);
    }
}
