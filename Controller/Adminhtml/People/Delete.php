<?php declare(strict_types=1);

namespace Adridope\People\Controller\Adminhtml\People;

use Adridope\People\Model\PeopleFactory;
use Adridope\People\Model\ResourceModel\People as PeopleResource;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Adridope_People::people_delete';

    /** @var PeopleFactory\ */
    protected $peopleFactory;

    /** @var PeopleResource */
    protected $peopleResource;

    public function __construct(
        Context $context,
        PeopleFactory $peopleFactory,
        PeopleResource $peopleResource
    ){
        $this->peopleFactory = $peopleFactory;
        $this->peopleResource =$peopleResource;
        parent::__construct($context);
    }

    public function execute() :Redirect
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $this->peopleFactory->create()
                    ->load($id)
                    ->delete();
                /*var_dump($this->getRequest()->getParam('id'));
                die();*/
                $this->messageManager->addSuccessMessage(__('The person has been deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $resultRedirect->setPath('*/*/edit', ['id' => $id]);

                return $resultRedirect;
            }
        } else {
            $this->messageManager->addErrorMessage(__('The person was not found.'));
        }

        $resultRedirect->setPath('*/*/');

        return $resultRedirect;
    }
}
