<?php

namespace Adridope\People\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class AboutUs extends Template implements BlockInterface
{
    protected $_template = "widget/aboutUs.phtml";
}
