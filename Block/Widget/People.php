<?php

namespace Adridope\People\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class People extends Template implements BlockInterface
{
    protected $_template = "widget/people.phtml";
    protected $_peopleFactory;
    public $_storeManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Adridope\People\Model\PeopleFactory $peopleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        $this->_peopleFactory = $peopleFactory;
        parent::__construct($context);
    }

    public function getPeopleCollection()
    {
        $people = $this->_peopleFactory->create();
        return $people->getCollection()->addFieldtoFilter('is_active', '1')->setOrder('sort_order', 'asc');
    }

    public function getPositions($item)
    {
        $storeId = $this->_storeManager->getStore()->getId();
        $position = $item->getPosition();
        switch ($storeId) {
            case 2:
                $position = $item->getPosition2();
                break;
            case 3:
                $position = $item->getPosition3();
                break;
        }

        return $position;
    }
}
