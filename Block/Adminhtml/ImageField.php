<?php

namespace Adridope\People\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Adridope\People\Block\Adminhtml\Renderer\ImageFieldRenderer;

class ImageField extends Template
{
    public function prepareElementHtml(AbstractElement $element)
    {
        $fieldRenderer = $this->getLayout()->createBlock(ImageFieldRenderer::class);
        $element->setRenderer($fieldRenderer);
    }
}
