<?php

namespace Adridope\People\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_peopleFactory;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Adridope\People\Model\PeopleFactory $peopleFactory
    )
    {
        $this->_peopleFactory = $peopleFactory;
        parent::__construct($context);
    }

    public function getPeopleCollection(){
        $people = $this->_peopleFactory->create();
        return $people->getCollection()->addFieldtoFilter('is_active','1')->setOrder('sort_order','asc');
    }
}
