<?php
namespace Adridope\People\Model\ResourceModel\People;

use Adridope\People\Model\People;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(People::class,\Adridope\People\Model\ResourceModel\People::class);
    }
}
